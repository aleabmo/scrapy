import scrapy


class DesSpider(scrapy.Spider):
    name = "prova"
    start_urls = [
        'http://www.venca.pt',
    ]

    def parse(self, response):
        for li in response.xpath("//@data-vc-node-id").extract():
            yield scrapy.FormRequest("https://www.venca.pt/Navigation/Node",
                                     formdata={'id': li, 'type':'DESKTOP'},
                                     callback=self.parse_url
                                     )

    def parse_url(self, response):
        for url in response.xpath("//@href").extract():
            yield scrapy.Request("https://www.venca.pt"+url,
                                 callback=self.parse_des,
                                 meta={'url': url}
            )

    def parse_des(self, response):
        wea = response.xpath("//div[@class='col-xs-6 col-sm-4 col-lg-4 col-md-4 productCounter']")
        if not wea:
            yield {
                'url': response.meta['url'],
                'description': "Mal"
            }
        else:
            print("No hi ha")
