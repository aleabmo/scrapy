import scrapy


class DesSpider(scrapy.Spider):
    name = "prova"
    start_urls = [
        'http://pre.venca.com/fr',
    ]

    def parse(self, response):
        for li in response.xpath("//@data-vc-node-id").extract():
            yield scrapy.FormRequest("https://pre.venca.com/fr/Navigation/Node",
                                     formdata={'id': li, 'type':'DESKTOP'},
                                     callback=self.parse_url
                                     )

    def parse_url(self, response):
        for url in response.xpath("//@href").extract():
            yield scrapy.Request("https://pre.venca.com/"+url,
                                 callback=self.parse_des,
                                 meta={'url': url}
            )

    def parse_des(self, response):
        yield {
            'url': response.meta['url'],
            'description': response.css("h2.storeFrontsDescriptions::text").extract()[0]
        }
