# -*- coding: utf-8 -*-
import scrapy


class VcesSpider(scrapy.Spider):
    name = 'listurl'
    allowed_domains = ['venca.es']

    def start_requests(self):
        urls = [
            'http://www.venca.es/g'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        for url in response.xpath("//@href").extract():
            yield {
                'url': url
            }



