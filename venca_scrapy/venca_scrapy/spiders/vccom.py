# -*- coding: utf-8 -*-
import scrapy

class VccomSpider(scrapy.Spider):
    name = 'vccom'
    allowed_domains = ['venca.com']

    def start_requests(self):
        urls = [
            'http://www.venca.com'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for li in response.xpath("//@data-vc-node-id").extract():
            yield scrapy.FormRequest("https://www.venca.com/Navigation/Node",
                                     formdata={'id': li, 'type':'DESKTOP'},
                                     callback=self.parse_url
                                     )

    def parse_url(self, response):
        for url in response.xpath("//@href").extract():
            yield scrapy.Request("https://www.venca.com"+url,
                                 callback=self.parse_des,
                                 meta={'url': url}
            )

    def parse_des(self, response):
        product = response.xpath("//div[@class='col-xs-6 col-sm-4 col-lg-4 col-md-4 productCounter']")
        if not product:
            yield {
                'url': response.meta['url'],
                'e': response.meta['url'].split("/")[-2],
                'slug': response.meta['url'].split("/")[-1]
            }
